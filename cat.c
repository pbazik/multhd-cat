#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>

#define N_THREADS 2
#define SIZE 2097152
#define INPUT_FD 0
#define OUTPUT_FD 1

char mem[SIZE];
size_t count = 0, r, w, EOF_FLAG = 0;

pthread_mutex_t mutex;
pthread_cond_t rcond;
pthread_cond_t wcond;


void* read_from_stdin(void* id)
{
    do
    {
        pthread_mutex_lock(&mutex);

        while (count) { pthread_cond_wait(&rcond, &mutex); }

        r = read(INPUT_FD, mem, SIZE);
        count = r;
        if (!r) EOF_FLAG = 1;

        pthread_cond_signal(&wcond);
        pthread_mutex_unlock(&mutex); 
    } while (r);
    
    pthread_exit(NULL);
    
}

void* write_to_stdout(void* id)
{
    do
    {
        pthread_mutex_lock(&mutex);

        while (!count) 
        { 
            if (EOF_FLAG) { pthread_exit(NULL); } 
            else pthread_cond_wait(&wcond, &mutex); 
        }
         
        w = write(OUTPUT_FD, mem, count);
        count = 0;

        pthread_cond_signal(&rcond);
        pthread_mutex_unlock(&mutex);
    } while (w);
    
    pthread_exit(NULL);
    
}

int main()
{
    pthread_t threads[N_THREADS];

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&rcond, NULL);
    pthread_cond_init(&wcond, NULL);


    for (int t = 0; t < N_THREADS/2; ++t)
    {
        pthread_create(&threads[t], NULL, read_from_stdin, (void*) (long)t);
    }

    for (int t = N_THREADS/2; t < N_THREADS; ++t)
    {
        pthread_create(&threads[t], NULL, write_to_stdout, (void*) (long)t);
    }

    for (int t = 0; t < N_THREADS; ++t)
    {
        pthread_join(threads[t], NULL);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&rcond);
    pthread_cond_destroy(&wcond);

    return 0;
}
